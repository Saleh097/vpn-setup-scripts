#!/usr/bin/bash

while getopts s: flag
do
    case "${flag}" in
        s) serverIP=${OPTARG};;
    esac
done

mkdir ghostunnel-client

cd ghostunnel-client

wget https://github.com/ghostunnel/ghostunnel/releases/download/v1.7.3/ghostunnel-linux-amd64

mv ghostunnel-linux-amd64 ghostunnel

./ghostunnel client \
    --listen localhost:8003 \
    --target ${serverIP}:8002 \
    --key gtkeys/clientkey.pem \
    --cert gtkeys/clientcert.pem \
    --cacert gtkeys/servercert.pem