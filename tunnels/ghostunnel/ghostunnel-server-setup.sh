#!/usr/bin/bash

# Get command arguments
while getopts s:c: flag
do
    case "${flag}" in
        s) serverIP=${OPTARG};;
        c) clientIP=${OPTARG};;
    esac
done

apt update && apt upgrade

apt install openjdk-19-jre-headless # To use for key generation

mkdir ~/ghostunnel_server_install

cd ~/ghostunnel_server_install

wget https://github.com/ghostunnel/ghostunnel/releases/download/v1.7.3/ghostunnel-linux-amd64       # Download ghostunnel executable

mv ghostunnel-linux-amd64 ghostunnel

chmod 755 ghostunnel

mkdir gtkeys    # Make directory to store keys

cd gtkeys

# Generate server certificate
keytool \
 -keystore server.jks  -storepass protected  -deststoretype pkcs12 \
 -genkeypair -keyalg RSA -validity 365 \
 -dname "CN=$serverIP" \
 -ext "SAN=IP:$serverIP"

openssl pkcs12 -in server.jks -nodes -nocerts -out serverkey.pem -password pass:protected       # Export the server certificate private key

openssl pkcs12 -in server.jks -nokeys -out servercert.pem -password pass:protected              # Export the server certificate public key

# Generate client certificate
keytool \
 -keystore client.jks  -storepass protected  -deststoretype pkcs12 \
 -genkeypair -keyalg RSA -validity 365 \
 -dname "CN=$clientIP" \
 -ext "SAN=IP:$clientIP"

openssl pkcs12 -in client.jks -nodes -nocerts -out clientkey.pem -password pass:protected       # Export the client certificate private key

openssl pkcs12 -in client.jks -nokeys -out clientcert.pem -password pass:protected              # Export the clinet certificate public key

nc -l localhost 8001 &   # Start server simulation on port 8081 using Netcat --- & is for setting command to run in background

# Start ghostunnel
~/ghostunnel_server_install/ghostunnel server \
    --listen ${serverIP}:8002 \
    --target localhost:8001 \
    --key ~/ghostunnel_server_install/gtkeys/serverkey.pem \
    --cert ~/ghostunnel_server_install/gtkeys/servercert.pem \
    --cacert ~/ghostunnel_server_install/gtkeys/clientcert.pem --allow-all &