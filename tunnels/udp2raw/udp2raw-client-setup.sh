#!/usr/bin/bash

# Get command arguments
while getopts s:i:o: flag
do
    case "${flag}" in
        s) serverIP=${OPTARG};;
        i) inputPort=${OPTARG};;
        o) outputPort=${OPTARG};;
    esac
done

mkdir ~/udp2raw

cd ~/udp2raw

wget https://github.com/wangyu-/udp2raw/releases/download/20230206.0/udp2raw_binaries.tar.gz

tar -xvf udp2raw_binaries.tar.gz -C ~/udp2raw

./udp2raw_amd64 -c -l 0.0.0.0:${inputPort}  -r ${serverIP}:${outputPort} -k "passwd" --raw-mode faketcp -a

