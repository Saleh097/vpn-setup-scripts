#!/usr/bin/bash

# Get command arguments
while getopts i:o: flag
do
    case "${flag}" in
        i) inputPort=${OPTARG};;
        o) outputPort=${OPTARG};;
    esac
done

mkdir ~/udp2raw

cd ~/udp2raw

wget https://github.com/wangyu-/udp2raw/releases/download/20230206.0/udp2raw_binaries.tar.gz

tar -xvf udp2raw_binaries.tar.gz -C ~/udp2raw

./udp2raw_amd64 -s -l 0.0.0.0:${inputPort} -r 127.0.0.1:${outputPort} -k "passwd" --raw-mode faketcp -a