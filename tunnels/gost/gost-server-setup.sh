#!/usr/bin/bash

# Get command arguments
while getopts i:o: flag
do
    case "${flag}" in
        i) inputPort=${OPTARG};;
        o) outputPort=${OPTARG};;
    esac
done

mkdir ~/gost_tunnel
cd ~/gost_tunnel

wget https://github.com/go-gost/gost/releases/download/v3.0.0-rc6/gost_3.0.0-rc6_linux_amd64.tar.gz

mkdir /usr/local/bin/gost

tar -xvzf gost_3.0.0-rc6_linux_amd64.tar.gz -C /usr/local/bin/gost/

chmod +x /usr/local/bin/gost/

cat > /usr/lib/systemd/system/gost.service << EOF
[Unit]
Description=GO Simple Tunnel
After=network.target
Wants=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/gost/gost -L=relay+wss://:${inputPort}/:${outputPort}

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload

systemctl start gost

# Alternatives

# -

# -L=kcp://:9000/:8083