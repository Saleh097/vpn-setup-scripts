#!/usr/bin/bash

# Simple porxy server used for testing tunnels

apt install squid

cat > /etc/squid/squid.conf << EOF
acl all src all
http_access allow all
http_port 3128
EOF

systemctl restart squid

systemctl stop squid    # Since all sources are allowed, enable it when needed
