#!/usr/bin/bash

while getopts i:s: flags
do
	case "${flags}" in
		i) serverIP=${OPTARG};;
		s) radSecret=${OPTARG};;
	esac
done


apt install freeradius freeradius-mysql freeradius-utils

apt install mysql-server

systemctl start mysql.service

mysql_secure_installation

mysql -uroot -e "CREATE DATABASE radius;"

mysql -uroot radius < /etc/freeradius/3.0/mods-config/sql/main/mysql/schema.sql
mysql -uroot radius </etc/freeradius/3.0/mods-config/sql/main/mysql/setup.sql

ln -s /etc/freeradius/3.0/mods-available/sql /etc/freeradius/3.0/mods-enabled/

echo 'sql {
	dialect = "mysql"
	driver = "rlm_sql_mysql"
	sqlite {
		filename = "/tmp/freeradius.db"
		busy_timeout = 200
		bootstrap = "${modconfdir}/${..:name}/main/sqlite/schema.sql"
	}
	mysql {
		tls {
		}
		warnings = auto
	}
	postgresql {
		send_application_name = yes
	}
	mongo {
		appname = "freeradius"
		tls {
			certificate_file = /path/to/file
			certificate_password = "password"
			ca_file = /path/to/file
			ca_dir = /path/to/directory
			crl_file = /path/to/file
			weak_cert_validation = false
			allow_invalid_hostname = false
		}
	}
	server = "localhost"
	port = 3306
	login = "radius"
	password = ""
	radius_db = "radius"
	acct_table1 = "radacct"
	acct_table2 = "radacct"
	postauth_table = "radpostauth"
	authcheck_table = "radcheck"
	groupcheck_table = "radgroupcheck"
	authreply_table = "radreply"
	groupreply_table = "radgroupreply"
	usergroup_table = "radusergroup"
	delete_stale_sessions = yes
	pool {
		start = ${thread[pool].start_servers}
		min = ${thread[pool].min_spare_servers}
		max = ${thread[pool].max_servers}
		spare = ${thread[pool].max_spare_servers}
		uses = 0
		retry_delay = 30
		lifetime = 0
		idle_timeout = 60
	}
	read_clients = yes
	client_table = "nas"
	group_attribute = "SQL-Group"
	$INCLUDE ${modconfdir}/${.:name}/main/${dialect}/queries.conf
}' > /etc/freeradius/3.0/mods-available/sql

echo 'server default {
listen {
	type = auth
	ipaddr = *
	port = 0
	limit {
	      max_connections = 16
	      lifetime = 0
	      idle_timeout = 30
	}
}
listen {
	ipaddr = *
	port = 0
	type = acct
	limit {
	}
}
listen {
	type = auth
	port = 0
	limit {
	      max_connections = 16
	      lifetime = 0
	      idle_timeout = 30
	}
}
listen {
	ipv6addr = ::
	port = 0
	type = acct
	limit {
	}
}
authorize {
	filter_username
	preprocess
	chap
	mschap
	digest
	suffix
	eap {
		ok = return
	}
	files
	sql
	-ldap
	expiration
	logintime
	pap
	Autz-Type New-TLS-Connection {
		  ok
	}
}
authenticate {
	Auth-Type PAP {
		pap
	}
	Auth-Type CHAP {
		chap
	}
	Auth-Type MS-CHAP {
		mschap
	}
	mschap
	digest
	eap
}
preacct {
	preprocess
	acct_unique
	suffix
	files
}
accounting {
	detail
	unix
	sql
	exec
	attr_filter.accounting_response
}
session {
	sql
}
post-auth {
	if (session-state:User-Name && reply:User-Name && request:User-Name && (reply:User-Name == request:User-Name)) {
		update reply {
			&User-Name !* ANY
		}
	}
	update {
		&reply: += &session-state:
	}
	sql
	exec
	remove_reply_message_if_eap
	Post-Auth-Type REJECT {
		-sql
		attr_filter.access_reject
		eap
		remove_reply_message_if_eap
	}
	Post-Auth-Type Challenge {
	}
	Post-Auth-Type Client-Lost {
	}
	if (EAP-Key-Name && &reply:EAP-Session-Id) {
		update reply {
			&EAP-Key-Name := &reply:EAP-Session-Id
		}
	}
}
pre-proxy {
}
post-proxy {
	eap
}
}' > /etc/freeradius/3.0/sites-enabled/default

echo '
CREATE TABLE IF NOT EXISTS radacct (
  radacctid bigint(21) NOT NULL auto_increment,
  acctsessionid varchar(64) NOT NULL default '',
  acctuniqueid varchar(32) NOT NULL default '',
  username varchar(64) NOT NULL default '',
  realm varchar(64) default '',
  nasipaddress varchar(15) NOT NULL default '',
  nasportid varchar(32) default NULL,
  nasporttype varchar(32) default NULL,
  acctstarttime datetime NULL default NULL,
  acctupdatetime datetime NULL default NULL,
  acctstoptime datetime NULL default NULL,
  acctinterval int(12) default NULL,
  acctsessiontime int(12) unsigned default NULL,
  acctauthentic varchar(32) default NULL,
  connectinfo_start varchar(128) default NULL,
  connectinfo_stop varchar(128) default NULL,
  acctinputoctets bigint(20) default NULL,
  acctoutputoctets bigint(20) default NULL,
  calledstationid varchar(50) NOT NULL default '',
  callingstationid varchar(50) NOT NULL default '',
  acctterminatecause varchar(32) NOT NULL default '',
  servicetype varchar(32) default NULL,
  framedprotocol varchar(32) default NULL,
  framedipaddress varchar(15) NOT NULL default '',
  framedipv6address varchar(45) NOT NULL default '',
  framedipv6prefix varchar(45) NOT NULL default '',
  framedinterfaceid varchar(44) NOT NULL default '',
  delegatedipv6prefix varchar(45) NOT NULL default '',
  class varchar(64) default NULL,
  PRIMARY KEY (radacctid),
  UNIQUE KEY acctuniqueid (acctuniqueid),
  KEY username (username),
  KEY framedipaddress (framedipaddress),
  KEY framedipv6address (framedipv6address),
  KEY framedipv6prefix (framedipv6prefix),
  KEY framedinterfaceid (framedinterfaceid),
  KEY delegatedipv6prefix (delegatedipv6prefix),
  KEY acctsessionid (acctsessionid),
  KEY acctsessiontime (acctsessiontime),
  KEY acctstarttime (acctstarttime),
  KEY acctinterval (acctinterval),
  KEY acctstoptime (acctstoptime),
  KEY nasipaddress (nasipaddress),
  KEY class (class)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS radcheck (
  id int(11) unsigned NOT NULL auto_increment,
  username varchar(64) NOT NULL default '',
  attribute varchar(64)  NOT NULL default '',
  op char(2) NOT NULL DEFAULT '==',
  value varchar(253) NOT NULL default '',
  is_active bool NOT NULL default 1,
  PRIMARY KEY  (id),
  KEY username (username(32))
);

CREATE TABLE IF NOT EXISTS radgroupcheck (
  id int(11) unsigned NOT NULL auto_increment,
  groupname varchar(64) NOT NULL default '',
  attribute varchar(64)  NOT NULL default '',
  op char(2) NOT NULL DEFAULT '==',
  value varchar(253)  NOT NULL default '',
  PRIMARY KEY  (id),
  KEY groupname (groupname(32))
);

CREATE TABLE IF NOT EXISTS radgroupreply (
  id int(11) unsigned NOT NULL auto_increment,
  groupname varchar(64) NOT NULL default '',
  attribute varchar(64)  NOT NULL default '',
  op char(2) NOT NULL DEFAULT '=',
  value varchar(253)  NOT NULL default '',
  PRIMARY KEY  (id),
  KEY groupname (groupname(32))
);

CREATE TABLE IF NOT EXISTS radreply (
  id int(11) unsigned NOT NULL auto_increment,
  username varchar(64) NOT NULL default '',
  attribute varchar(64) NOT NULL default '',
  op char(2) NOT NULL DEFAULT '=',
  value varchar(253) NOT NULL default '',
  PRIMARY KEY  (id),
  KEY username (username(32))
);

CREATE TABLE IF NOT EXISTS radusergroup (
  id int(11) unsigned NOT NULL auto_increment,
  username varchar(64) NOT NULL default '',
  groupname varchar(64) NOT NULL default '',
  priority int(11) NOT NULL default '1',
  PRIMARY KEY  (id),
  KEY username (username(32))
);

CREATE TABLE IF NOT EXISTS radpostauth (
  id int(11) NOT NULL auto_increment,
  username varchar(64) NOT NULL default '',
  pass varchar(64) NOT NULL default '',
  reply varchar(32) NOT NULL default '',
  authdate timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  class varchar(64) default NULL,
  PRIMARY KEY  (id),
  KEY username (username),
  KEY class (class)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS nas (
  id int(10) NOT NULL auto_increment,
  nasname varchar(128) NOT NULL,
  shortname varchar(32),
  type varchar(30) DEFAULT 'other',
  ports int(5),
  secret varchar(60) DEFAULT 'secret' NOT NULL,
  server varchar(64),
  community varchar(50),
  description varchar(200) DEFAULT 'RADIUS Client',
  PRIMARY KEY (id),
  KEY nasname (nasname)
);
' > /etc/freeradius/3.0/mods-config/sql/main/mysql/schema.sql

echo '
safe_characters = "@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_: /"
sql_user_name = "%{User-Name}"
event_timestamp_epoch = "%{%{integer:Event-Timestamp}:-%l}"
event_timestamp = "FROM_UNIXTIME(${event_timestamp_epoch})"

class {
	column_name =	# ", class"
	packet_xlat =	# ", '%{Class}'"
	reply_xlat =	# ", '%{reply:Class}'"
}

client_query = "\
	SELECT id, nasname, shortname, type, secret, server \
	FROM ${client_table}"

authorize_check_query = "\
	SELECT id, username, attribute, value, op \
	FROM ${authcheck_table} AND is_active = 1 \
	WHERE username = '%{SQL-User-Name}' \
	ORDER BY id"

authorize_reply_query = "\
	SELECT id, username, attribute, value, op \
	FROM ${authreply_table} \
	WHERE username = '%{SQL-User-Name}' \
	ORDER BY id"

group_membership_query = "\
	SELECT groupname \
	FROM ${usergroup_table} \
	WHERE username = '%{SQL-User-Name}' \
	ORDER BY priority"

authorize_group_check_query = "\
	SELECT id, groupname, attribute, \
	Value, op \
	FROM ${groupcheck_table} \
	WHERE groupname = '%{${group_attribute}}' \
	ORDER BY id"

authorize_group_reply_query = "\
	SELECT id, groupname, attribute, \
	value, op \
	FROM ${groupreply_table} \
	WHERE groupname = '%{${group_attribute}}' \
	ORDER BY id"

simul_count_query = "\
	SELECT COUNT(*) \
	FROM ${acct_table1} \
	WHERE username = '%{SQL-User-Name}' \
	AND acctstoptime IS NULL"

simul_verify_query = "\
	SELECT \
		radacctid, acctsessionid, username, nasipaddress, nasportid, framedipaddress, \
		callingstationid, framedprotocol \
	FROM ${acct_table1} \
	WHERE username = '%{SQL-User-Name}' \
	AND acctstoptime IS NULL"

accounting {
	reference = "%{tolower:type.%{%{Acct-Status-Type}:-%{Request-Processing-Stage}}.query}"

	column_list = "\
		acctsessionid,		acctuniqueid,		username, \
		realm,			nasipaddress,		nasportid, \
		nasporttype,		acctstarttime,		acctupdatetime, \
		acctstoptime,		acctsessiontime, 	acctauthentic, \
		connectinfo_start,	connectinfo_stop, 	acctinputoctets, \
		acctoutputoctets,	calledstationid, 	callingstationid, \
		acctterminatecause,	servicetype,		framedprotocol, \
		framedipaddress,	framedipv6address,	framedipv6prefix, \
		framedinterfaceid,	delegatedipv6prefix	${..class.column_name}"

	type {
		accounting-on {
			query = "\
				UPDATE ${....acct_table1} \
				SET \
					acctstoptime = ${....event_timestamp}, \
					acctsessiontime	= '${....event_timestamp_epoch}' \
						- UNIX_TIMESTAMP(acctstarttime), \
					acctterminatecause = '%{%{Acct-Terminate-Cause}:-NAS-Reboot}' \
				WHERE acctstoptime IS NULL \
				AND nasipaddress   = '%{NAS-IP-Address}' \
				AND acctstarttime <= ${....event_timestamp}"
		}

		accounting-off {
			query = "${..accounting-on.query}"
		}

		post-auth {
			query = "\
			INSERT INTO ${....acct_table1} \
				(${...column_list}) \
			VALUES(\
				'%{Acct-Session-Id}', \
				'%{Acct-Unique-Session-Id}', \
				'%{SQL-User-Name}', \
				'%{Realm}', \
				'%{%{NAS-IPv6-Address}:-%{NAS-IP-Address}}', \
				NULLIF('%{%{NAS-Port-ID}:-%{NAS-Port}}', ''), \
				'%{NAS-Port-Type}', \
				${....event_timestamp}, \
				NULL, \
				NULL, \
				0, \
				'', \
				'%{Connect-Info}', \
				NULL, \
				0, \
				0, \
				'%{Called-Station-Id}', \
				'%{Calling-Station-Id}', \
				'', \
				'%{Service-Type}', \
				NULL, \
				'', \
				'', \
				'', \
				'', \
				'' \
				${....class.packet_xlat})"

			query = "\
				UPDATE ${....acct_table1} SET \
					AcctStartTime = ${....event_timestamp}, \
					AcctUpdateTime = ${....event_timestamp}, \
					ConnectInfo_start = '%{Connect-Info}', \
					AcctSessionId = '%{Acct-Session-Id}' \
				WHERE UserName = '%{SQL-User-Name}' \
					AND NASIPAddress = '%{%{NAS-IPv6-Address}:-%{NAS-IP-Address}}' \
					AND NASPortId = '%{%{NAS-Port-ID}:-%{NAS-Port}}' \
					AND NASPortType = '%{NAS-Port-Type}' \
					AND AcctStopTime IS NULL"
		}

		start {
			query = "\
				INSERT INTO ${....acct_table1} \
					(${...column_list}) \
				VALUES \
					('%{Acct-Session-Id}', \
					'%{Acct-Unique-Session-Id}', \
					'%{SQL-User-Name}', \
					'%{Realm}', \
					'%{NAS-IP-Address}', \
					'%{%{NAS-Port-ID}:-%{NAS-Port}}', \
					'%{NAS-Port-Type}', \
					${....event_timestamp}, \
					${....event_timestamp}, \
					NULL, \
					'0', \
					'%{Acct-Authentic}', \
					'%{Connect-Info}', \
					'', \
					'0', \
					'0', \
					'%{Called-Station-Id}', \
					'%{Calling-Station-Id}', \
					'', \
					'%{Service-Type}', \
					'%{Framed-Protocol}', \
					'%{Framed-IP-Address}', \
					'%{Framed-IPv6-Address}', \
					'%{Framed-IPv6-Prefix}', \
					'%{Framed-Interface-Id}', \
					'%{Delegated-IPv6-Prefix}' \
					${....class.packet_xlat})"

			-query = "\
				UPDATE ${....acct_table1} \
				SET \
					AcctSessionId = '%{Acct-Session-Id}', \
					AcctUniqueId = '%{Acct-Unique-Session-Id}', \
					AcctAuthentic = '%{Acct-Authentic}', \
					ConnectInfo_start = '%{Connect-Info}', \
					ServiceType = '%{Service-Type}', \
					FramedProtocol = '%{Framed-Protocol}', \
					framedipaddress = '%{Framed-IP-Address}', \
					framedipv6address = '%{Framed-IPv6-Address}', \
					framedipv6prefix = '%{Framed-IPv6-Prefix}', \
					framedinterfaceid = '%{Framed-Interface-Id}', \
					delegatedipv6prefix = '%{Delegated-IPv6-Prefix}', \
					AcctStartTime = ${....event_timestamp}, \
					AcctUpdateTime = ${....event_timestamp} \
				WHERE UserName = '%{SQL-User-Name}' \
					AND NASIPAddress = '%{%{NAS-IPv6-Address}:-%{NAS-IP-Address}}' \
					AND NASPortId = '%{%{NAS-Port-ID}:-%{NAS-Port}}' \
					AND NASPortType = '%{NAS-Port-Type}' \
					AND AcctStopTime IS NULL"
			query = "\
				UPDATE ${....acct_table1} SET \
					acctstarttime	= ${....event_timestamp}, \
					acctupdatetime	= ${....event_timestamp}, \
					connectinfo_start = '%{Connect-Info}' \
				WHERE AcctUniqueId = '%{Acct-Unique-Session-Id}'"

		}

		interim-update {
			query = "\
				UPDATE ${....acct_table1} \
				SET \
					acctupdatetime  = (@acctupdatetime_old:=acctupdatetime), \
					acctupdatetime  = ${....event_timestamp}, \
					acctinterval    = ${....event_timestamp_epoch} - \
						UNIX_TIMESTAMP(@acctupdatetime_old), \
					framedipaddress = '%{Framed-IP-Address}', \
					framedipv6address = '%{Framed-IPv6-Address}', \
					framedipv6prefix = '%{Framed-IPv6-Prefix}', \
					framedinterfaceid = '%{Framed-Interface-Id}', \
					delegatedipv6prefix = '%{Delegated-IPv6-Prefix}', \
					acctsessiontime = %{%{Acct-Session-Time}:-NULL}, \
					acctinputoctets = '%{%{Acct-Input-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Input-Octets}:-0}', \
					acctoutputoctets = '%{%{Acct-Output-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Output-Octets}:-0}' \
				WHERE AcctUniqueId = '%{Acct-Unique-Session-Id}'"

			query = "\
				INSERT INTO ${....acct_table1} \
					(${...column_list}) \
				VALUES \
					('%{Acct-Session-Id}', \
					'%{Acct-Unique-Session-Id}', \
					'%{SQL-User-Name}', \
					'%{Realm}', \
					'%{NAS-IP-Address}', \
					'%{%{NAS-Port-ID}:-%{NAS-Port}}', \
					'%{NAS-Port-Type}', \
					FROM_UNIXTIME(${....event_timestamp_epoch} - %{%{Acct-Session-Time}:-0}), \
					${....event_timestamp}, \
					NULL, \
					%{%{Acct-Session-Time}:-NULL}, \
					'%{Acct-Authentic}', \
					'%{Connect-Info}', \
					'', \
					'%{%{Acct-Input-Gigawords}:-0}' << 32 | '%{%{Acct-Input-Octets}:-0}', \
					'%{%{Acct-Output-Gigawords}:-0}' << 32 | '%{%{Acct-Output-Octets}:-0}', \
					'%{Called-Station-Id}', \
					'%{Calling-Station-Id}', \
					'', \
					'%{Service-Type}', \
					'%{Framed-Protocol}', \
					'%{Framed-IP-Address}', \
					'%{Framed-IPv6-Address}', \
					'%{Framed-IPv6-Prefix}', \
					'%{Framed-Interface-Id}', \
					'%{Delegated-IPv6-Prefix}' \
					${....class.packet_xlat})"

			-query = "\
				UPDATE ${....acct_table1} \
				SET \
					AcctSessionId = '%{Acct-Session-Id}', \
					AcctUniqueId = '%{Acct-Unique-Session-Id}', \
					AcctAuthentic = '%{Acct-Authentic}', \
					ConnectInfo_start = '%{Connect-Info}', \
					ServiceType = '%{Service-Type}', \
					FramedProtocol = '%{Framed-Protocol}', \
					framedipaddress = '%{Framed-IP-Address}', \
					framedipv6address = '%{Framed-IPv6-Address}', \
					framedipv6prefix = '%{Framed-IPv6-Prefix}', \
					framedinterfaceid = '%{Framed-Interface-Id}', \
					delegatedipv6prefix = '%{Delegated-IPv6-Prefix}', \
					AcctUpdateTime = ${....event_timestamp}, \
					AcctSessionTime = %{%{Acct-Session-Time}:-NULL}, \
					AcctInputOctets = '%{%{Acct-Input-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Input-Octets}:-0}', \
					AcctOutputOctets = '%{%{Acct-Output-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Output-Octets}:-0}' \
				WHERE UserName = '%{SQL-User-Name}' \
					AND NASIPAddress = '%{%{NAS-IPv6-Address}:-%{NAS-IP-Address}}' \
					AND NASPortId = '%{%{NAS-Port-ID}:-%{NAS-Port}}' \
					AND NASPortType = '%{NAS-Port-Type}' \
					AND AcctStopTime IS NULL"

		}

		stop {
			query = "\
				UPDATE ${....acct_table2} SET \
					acctstoptime	= ${....event_timestamp}, \
					acctsessiontime	= %{%{Acct-Session-Time}:-NULL}, \
					acctinputoctets	= '%{%{Acct-Input-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Input-Octets}:-0}', \
					acctoutputoctets = '%{%{Acct-Output-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Output-Octets}:-0}', \
					acctterminatecause = '%{Acct-Terminate-Cause}', \
					connectinfo_stop = '%{Connect-Info}' \
				WHERE AcctUniqueId = '%{Acct-Unique-Session-Id}'"

			query = "\
				INSERT INTO ${....acct_table2} \
					(${...column_list}) \
				VALUES \
					('%{Acct-Session-Id}', \
					'%{Acct-Unique-Session-Id}', \
					'%{SQL-User-Name}', \
					'%{Realm}', \
					'%{NAS-IP-Address}', \
					'%{%{NAS-Port-ID}:-%{NAS-Port}}', \
					'%{NAS-Port-Type}', \
					FROM_UNIXTIME(${....event_timestamp_epoch} - %{%{Acct-Session-Time}:-0}), \
					${....event_timestamp}, \
					${....event_timestamp}, \
					%{%{Acct-Session-Time}:-NULL}, \
					'%{Acct-Authentic}', \
					'', \
					'%{Connect-Info}', \
					'%{%{Acct-Input-Gigawords}:-0}' << 32 | '%{%{Acct-Input-Octets}:-0}', \
					'%{%{Acct-Output-Gigawords}:-0}' << 32 | '%{%{Acct-Output-Octets}:-0}', \
					'%{Called-Station-Id}', \
					'%{Calling-Station-Id}', \
					'%{Acct-Terminate-Cause}', \
					'%{Service-Type}', \
					'%{Framed-Protocol}', \
					'%{Framed-IP-Address}', \
					'%{Framed-IPv6-Address}', \
					'%{Framed-IPv6-Prefix}', \
					'%{Framed-Interface-Id}', \
					'%{Delegated-IPv6-Prefix}' \
					${....class.packet_xlat})"

			-query = "\
				UPDATE ${....acct_table1} \
				SET \
					AcctSessionId = '%{Acct-Session-Id}', \
					AcctUniqueId = '%{Acct-Unique-Session-Id}', \
					AcctAuthentic = '%{Acct-Authentic}', \
					ConnectInfo_start = '%{Connect-Info}', \
					ServiceType = '%{Service-Type}', \
					FramedProtocol = '%{Framed-Protocol}', \
					framedipaddress = '%{Framed-IP-Address}', \
					framedipv6address = '%{Framed-IPv6-Address}', \
					framedipv6prefix = '%{Framed-IPv6-Prefix}', \
					framedinterfaceid = '%{Framed-Interface-Id}', \
					delegatedipv6prefix = '%{Delegated-IPv6-Prefix}', \
					AcctStopTime = ${....event_timestamp}, \
					AcctUpdateTime = ${....event_timestamp}, \
					AcctSessionTime = %{Acct-Session-Time}, \
					AcctInputOctets = '%{%{Acct-Input-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Input-Octets}:-0}', \
					AcctOutputOctets = '%{%{Acct-Output-Gigawords}:-0}' \
						<< 32 | '%{%{Acct-Output-Octets}:-0}', \
					AcctTerminateCause = '%{Acct-Terminate-Cause}', \
					ConnectInfo_stop = '%{Connect-Info}' \
				WHERE UserName = '%{SQL-User-Name}' \
				AND NASIPAddress = '%{%{NAS-IPv6-Address}:-%{NAS-IP-Address}}' \
				AND NASPortId = '%{%{NAS-Port-ID}:-%{NAS-Port}}' \
				AND NASPortType = '%{NAS-Port-Type}' \
				AND AcctStopTime IS NULL"

		}

		accounting {
			query = "SELECT true"
		}
	}
}

post-auth {

	query =	"\
		INSERT INTO ${..postauth_table} \
			(username, pass, reply, authdate ${..class.column_name}) \
		VALUES ( \
			'%{SQL-User-Name}', \
			'%{%{User-Password}:-%{Chap-Password}}', \
			'%{reply:Packet-Type}', \
			'%S.%M' \
			${..class.reply_xlat})"
}
' > /etc/freeradius/3.0/mods-config/sql/main/mysql/queries.conf

mysql -uroot radius -e "INSERT INTO nas VALUES (NULL, '$serverIP', 'nascl', 'other', NULL, '$radSecret', 'default', NULL, 'RADIUS Client');"

systemctl start freeradius.service