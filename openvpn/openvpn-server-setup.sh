#!/usr/bin/bash

while getops k:i:p: flag
do 
	case "${flag}" in
		k) activationKey=${OPTARG};;
		i) serverIP=${OPTARG};;
		p) radSecret=${OPTARG};;
	esac
done

# Install updates and set the correct time
apt update
apt upgrade
apt install tzdata
dpkg-reconfigure tzdata


# Install via Repository
apt update && apt -y install ca-certificates wget net-tools gnupg
wget https://as-repository.openvpn.net/as-repo-public.asc -qO /etc/apt/trusted.gpg.d/as-repository.asc
echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/as-repository.asc] http://as-repository.openvpn.net/as/debian jammy main">/etc/apt/sources.list.d/openvpn-as-repo.list
apt update && apt -y install openvpn-as > creds.txt


# Bootstrap sacli
/usr/local/openvpn_as/scripts/sacli ConfigQuery


# Activating subscription
sacli -v "$activationKey" LoadSubscription
sacli SubscriptionStatus > subscription_stats.txt


# Create SSL Keys
apt-get install openssl
openssl req -out server.csr -new -newkey rsa:4096 -sha256 -nodes -keyout server.key


# Set up Radius Auth
sacli --key "auth.radius.0.auth_method" --value "pap" ConfigPut
sacli --key "auth.radius.0.name" --value "nascl" ConfigPut
sacli --key "auth.radius.0.server.0.host" --value "$serverIP" ConfigPut
sacli --key "auth.radius.0.server.0.secret" --value "$radSecret" ConfigPut
sacli --key "auth.radius.0.server.0.auth_port" --value "1812" ConfigPut
sacli --key "auth.radius.0.server.0.acct_port" --value "1813" ConfigPut
sacli start

sacli --key "auth.radius.0.enable" --value "True" ConfigPut
sacli start

sacli --key "auth.module.type" --value "radius" ConfigPut
sacli start