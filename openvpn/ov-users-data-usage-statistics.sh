#!/usr/bin/bash

sqlite3 -json /usr/local/openvpn_as/etc/db/log.db 'SELECT username, SUM(bytes_total) as used_traffic FROM log GROUP BY username;' > openvpn_users_data_use.json