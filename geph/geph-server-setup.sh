# git clone https://github.com/geph-official/geph5.git

# cd geph5

# bash deploy-exit.sh

mkdir ~/geph4-installation

cd geph4-installation

git clone https://github.com/geph-official/geph4-exit.git

apt update

apt install -y cargo

cd geph4-exit

cargo install --locked geph4-exit

export PATH="/root/.cargo/bin:$PATH"

mkdir /home/user/
touch /home/user/geph4-exit.key

echo '# Where to listen for incoming connections. Change 8814 to whatever port you like
sosistab_listen = "[::]:8814"
# Where to store secret key
secret_key = "/home/user/geph4-exit.key"' > /etc/geph4-exit.toml

# geph4-exit --config /etc/geph4-exit.toml

echo "[Unit]
Description=Geph4 exit service.

[Service]
Type=exec
Restart=always
ExecStart=geph4-exit --config /etc/geph4-exit.toml
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/geph4-exit.service

systemctl enable geph4-exit
systemctl daemon-reload
systemctl start geph4-exit
systemctl enable geph4-exit