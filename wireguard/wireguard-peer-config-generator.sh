#!/usr/bin/bash

while getopts i:d:p:k: flag
do
    case "${flag}" in 
        i) userIndex=${OPTARG};;
        d) domain=${OPTARG};;
        p) port=${OPTARG};;
        k) srvPublicKey=${OPTARG};;
    esac
done

userPrivateKey=$(wg genkey)
userPublicKey=$(echo $userPrivateKey | wg pubkey)

# create user config file
    cat > client$userIndex.conf << EOF
[Interface]
Address = 10.0.0.${userIndex}/24
ListenPort = ${port}
PrivateKey = ${userPrivateKey}
DNS = 8.8.8.8

[Peer]
PublicKey = ${srvPublicKey}
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = ${domain}:${port}
EOF


# show server config appendix

echo "
[Peer]
PublicKey = ${userPublicKey}
AllowedIPs = 10.0.0.${userIndex}/32"