#!/usr/bin/bash

apt update
apt upgrade

apt install wireguard

touch /etc/wireguard/wg0.conf

echo "net.ipv4.ip_forward=1" > /etc/sysctl.conf

sysctl -p

wg-quick up /etc/wireguard/wg0.conf