#!/usr/bin/bash

which jq || { echo "jq required"; exit 1; }

wg show all dump | \
  jq -R '
  split("\t") | select(length > 5) | {
    interface:  .[0],
    publickey:  .[1],
    public_ip:  .[3] | split(":")[0],
    private_ip: .[4] | split("/")[0],
    transferred_traffic: (.[6] | tonumber),
    received_traffic: (.[7] | tonumber),
    query_time: now | todateiso8601
  }' > wireguard.0


grep AllowedIPs /etc/wireguard/wg0.conf | grep -E '[A-Z]{2}-[A-Z]{2}' | jq -R '
  split("/") | {
    hostname:   .[3] | split(".")[0],
    private_ip: .[3] | split(" ")[2]
   }' > wireguard.1

jq -sc '[group_by(.private_ip)[]|add]' wireguard.0 wireguard.1 > wireguard.json