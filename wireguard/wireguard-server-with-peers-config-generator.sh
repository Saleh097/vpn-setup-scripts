#!/usr/bin/bash

# get inputs
while getopts d:p:n: flags
do
    case "${flags}" in
        d) serverDomain=${OPTARG};;
        p) servicePort=${OPTARG};;
        n) numberOfUsers=${OPTARG};;
    esac
done


# initialize server config file
serverPrivateKey=$(wg genkey)

serverPublicKey=$(echo $serverPrivateKey | wg pubkey)

cat > w0.conf << EOF 
[Interface]
Address = 10.0.0.1/24
ListenPort = ${servicePort}
PrivateKey = ${serverPrivateKey}
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
SaveConfig = true
EOF


# start creating user config files
clientIndex=2

while [ $clientIndex -le $[$numberOfUsers+1] ]
do
    clientPrivateKey=$(wg genkey)
    clientPublicKey=$(echo $clientPrivateKey | wg pubkey)

# make client config
    cat > client$[$clientIndex-1].conf << EOF
[Interface]
Address = 10.0.0.${clientIndex}/24
ListenPort = ${servicePort}
PrivateKey = ${clientPrivateKey}
DNS = 8.8.8.8

[Peer]
PublicKey = ${serverPublicKey}
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = ${serverDomain}:${servicePort}
EOF


# append client to server config file
    cat >> w0.conf << EOF 

[Peer]
PublicKey = ${clientPublicKey}
AllowedIPs = 10.0.0.${clientIndex}/32
EOF

clientIndex=$[$clientIndex+1]

done