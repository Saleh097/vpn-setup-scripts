#!/usr/bin/bash

while getopts u:i: flag
do
    case "${flag}" in
        u) serverUrl=${OPTARG};;
        i) serverIP=${OPTARG};;
    esac
done

cd ~

apt update
apt install nginx certbot python3-certbot-nginx
snap install go --classic

echo "server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www/html;

        index index.html index.htm index.nginx-debian.html;

        server_name $serverUrl;

        location / {
                try_files \$uri \$uri/ =404;
        }
}" > /etc/nginx/sites-enabled/default

certbot --nginx -d $serverUrl

git clone https://github.com/bepass-org/smartSNI.git
cd smartSNI

echo "{
  \"host\": \"$serverUrl\",
  \"domains\": {
    \"oracle.com\": \"$serverIP\",
    \"udemy.com\": \"$serverIP\"
  }
}" > config.json

go build

./smartSNI